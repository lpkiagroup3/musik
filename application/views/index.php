<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Musik</title>
<meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 	<link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" />  	  		
 	<!-- boostrap -->

	<link rel="stylesheet" href="<?= base_url() ?>assets/animate.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/style.css">


	

</head>
<body>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=249078091804020&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Header Starts -->
<div class="navbar-wrapper ">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top animated fadeInDown" role="navigation" id="top-nav">
          <div class="container">
            <div class="navbar-header">
              <!-- Logo Starts -->
              <a class="navbar-brand" href="#home"><img src="<?= base_url() ?>assets/images/logo.png" height="30" alt="logo"/></a>
              <!-- #Logo Ends -->


              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

            </div>

          </div>
        </div>

      </div>
    </div>
<!-- #Header Starts -->







<!-- overlay -->
<div class="container overlay">


<!-- home banner starts -->
<div id="home" class="homeinfo">
<div class="row">
	<div class="col-sm-3 col-xs-12"></div>
	<div class="col-sm-6 col-xs-12">
	<div class="fronttext">
	 	<h2 class="bgcolor  animated fadeInUpBig"><span class="glyphicon glyphicon-headphones"></span> <a href="<?php echo base_url('home');?>" class="btn btn-link btn-group-lg"><h2>MUSIC</h2></a></h2><br>
		<p class=" animated fadeInUp">All Music You Can Get!. Klik on "MUSIC" to Enter Homepage</p>
	</div>
	</div>

	<div class="col-sm-9 col-xs-12">
	<div class="player">
	<img src="<?= base_url() ?>assets/images/dj.png" class="graphics hidden-xs  animated fadeInRightBig" alt="dj"/>
		<iframe width="600" height="160" scrolling="no" frameborder="no" src="<?= base_url() ?>assets/images/feat.png"></iframe>
		</div>
	</div>

</div>	
</div>
<!-- home banner ends -->


</div>
<!-- overlay -->



<!-- Footer Starts -->
<div id="footer">
<div class="container">
<center> Copyright 2017 Musik. </center>
</div>
</div>
<!-- # Footer Ends -->

<!-- background slider -->
<div id="myCarousel" class="carousel slide hidden-xs">
<div class="carousel-inner">
    <div class="active item"><img src="<?= base_url() ?>ssets/images/back1.jpg" alt="" /></div>
    <div class="item"><img src="<?= base_url() ?>assets/images/back2.jpg" alt="" /></div>
    <div class="item"><img src="<?= base_url() ?>assets/images/back3.jpg" alt="" /></div>
  </div>
</div>
<!-- background slider -->



  <script src="http://code.jquery.com/jquery-1.7.1.min.js" type="text/javascript" ></script>
  <!-- boostrap -->
  <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js" type="text/javascript" ></script>
  <script src="<?= base_url() ?>assets/scripts/plugins.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/scripts/script.js" type="text/javascript"></script>

</body>
</html>

