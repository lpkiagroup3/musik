<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Musik</title>
<meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 	<link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" />  	  		
 	<!-- boostrap -->

	<link rel="stylesheet" href="<?= base_url() ?>assets/animate.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/style.css">


	

</head>
<body>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=249078091804020&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Header Starts -->
<div class="navbar-wrapper ">
      <div class="container">

        <div class="navbar navbar-inverse navbar-fixed-top animated fadeInDown" role="navigation" id="top-nav">
          <div class="container">
            <div class="navbar-header">
              <!-- Logo Starts -->
              <a class="navbar-brand" href="#home"><img src="<?= base_url() ?>assets/images/logo.png" height="30" alt="logo"/></a>
              <!-- #Logo Ends -->


              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

            </div>

			<div class="col-sm-3 col-md-3">
          <form class="navbar-form" role="search" action="<?= base_url('cari')?>">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="keyword">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
    </div>
            <!-- Nav Starts -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right" >
                 <li class="active"><a href="<?php base_url('home');?>">Home</a></li>
                 <li><a href="<?= base_url() ?>mood">Mood</a></li>
                 <li><a href="<?= base_url('daftar') ?>">Daftar Music</a></li>
                 <li><a href="<?= base_url('favorit') ?>">Favorit</a></li>
                 <li><a href="<?= base_url('bahasa') ?>">Bahasa</a></li>
                 <li><a href="<?= base_url('live') ?>">Live Streaming</a></li>
              </ul>
            </div>
            <!-- #Nav Ends -->

          </div>
        </div>

      </div>
    </div>
<!-- #Header Starts -->







<!-- overlay -->
<div class="container overlay">


<!-- home banner starts -->
<div id="home" class="homeinfo">
<div class="row">
	<div class="col-sm-6 col-xs-12">
	<div class="fronttext">
	 	<h2 class="bgcolor  animated fadeInUpBig"><span class="glyphicon glyphicon-headphones"></span> M U S I K</h2><br>
		<p class=" animated fadeInUp">Best Music Website!. There are other and natural causes tending toward a diminution of population, but nothing contributes so greatly to this end as the fact that no web or app Martian is ever voluntarily without a weapon of destruction.</p>
	</div>
	</div>
	<div class="col-sm-5 col-xs-12 col-sm-offset-1">
	<div class="player">
	<img src="<?= base_url() ?>assets/images/dj.png" class="graphics hidden-xs  animated fadeInRightBig" alt="dj"/>
		<iframe width="100%" height="170" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/247620599&amp;color=f2ab00&amp;theme_color=000000&amp;auto_play=true&amp;hide_related=true&amp;show_artwork=false"></iframe>
		</div>
	</div>

</div>	
</div>
<!-- home banner ends -->



<!-- blockblack -->
<div class="blockblack">

<!-- About Starts -->
<div id="about" class="spacer">
<h3><span class="glyphicon glyphicon-user"></span> About US</h3>
<div class="row">				
<div class="col-lg-4 col-sm-4  col-xs-12">
<img src="<?= base_url() ?>assets/images/4.jpg" class="img-responsive" alt="about"/>
</div>
<div class="col-lg-5 col-sm-8  col-xs-12">
<p>We serve our clients through a number of services, including effective web design, web application development, print design, online marketing and brand development. We serve our clients through a number of services, including effective webh5 design, web application development, print design, online marketing and brand development.</p>
<blockquote>We serve our clients through a number of services, including effective web design, web application development, print design, online marketing and brand development.</blockquote>
</div>
<div class="col-lg-3 visible-lg">
<div class="fb-like-box" data-href="http://www.facebook.com/LPKIA" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false" data-width="255"></div>
</div>
</div>
</div>
<!-- About Ends -->



</div>
<!-- blockblack -->

</div>
<!-- overlay -->



<!-- Footer Starts -->
<div id="footer">
<div class="container">
<center>Copyright 2017 Musik.</center>
</div>
</div>
<!-- # Footer Ends -->


<!-- background slider -->
<div id="myCarousel" class="carousel slide hidden-xs">
<div class="carousel-inner">
    <div class="active item"><img src="<?= base_url() ?>ssets/images/back1.jpg" alt="" /></div>
    <div class="item"><img src="<?= base_url() ?>assets/images/back2.jpg" alt="" /></div>
    <div class="item"><img src="<?= base_url() ?>assets/images/back3.jpg" alt="" /></div>
  </div>
</div>
<!-- background slider -->










  <script src="http://code.jquery.com/jquery-1.7.1.min.js" type="text/javascript" ></script>
  <!-- boostrap -->
  <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js" type="text/javascript" ></script>
  <script src="<?= base_url() ?>assets/scripts/plugins.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>assets/scripts/script.js" type="text/javascript"></script>

</body>
</html>

