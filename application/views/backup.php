


<!-- playlist Starts -->
<div id="playlist" class="spacer">
	<div class="row">
					<div class="col-md-12 col-xs-12">
					<h3><span class="glyphicon glyphicon-list"></span> Playlist</h3>
					<iframe width="100%" height="600" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/24314082&amp;theme_color=000000&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false"></iframe>
					</div>
	</div>
</div>
<!-- #playlist Ends -->





<!-- latest release starts-->
<div id="album" class="releases spacer">
<h3><span class="glyphicon glyphicon-music"></span> Album Releases</h3>
<div class="row">
	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/3.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>

	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/1.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>

	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/2.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>

	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/4.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>

	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/4.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>

	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/2.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>

	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/3.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>

	<div class="col-sm-3 col-xs-12"><div class="album"><img src="<?= base_url() ?>assets/images/1.jpg" class="img-responsive" alt="music theme" /><div class="albumdetail"><h5>Crazy Freso</h5><a href="#" class="listen" data-toggle="modal" data-target="#albumdetail"><span class="glyphicon glyphicon-headphones"></span> Listen Song</a></div></div></div>
</div>
</div>
<!-- latest release ends-->










<!--Blog Event Starts-->
<div id="blogevent"  class="blogevent spacer">
<div class="row">

	<!-- events -->
	<div class="col-md-6 col-xs-12">
		<div class="events">
		<h3><span class="glyphicon glyphicon-calendar"></span> Events</h3>
		<ul>
			<li>
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-lg-4"><a href="#" data-toggle="modal" data-target="#blogdetail"><img src="<?= base_url() ?>assets/images/1.jpg" class="img-responsive" alt="music theme" /></a></div>
				<div class="col-xs-12  col-sm-6 col-lg-5 "><h5><a href="#" data-toggle="modal" data-target="#blogdetail">Christmas Eve Party</a></h5><p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future.</p></div>
				<div class="col-xs-12  col-sm-3 col-lg-3 date"><b>2014</b><span>Dec 28</span></div>
			</div>
			</li>
			<li>
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-lg-4"><a href="#" data-toggle="modal" data-target="#blogdetail"><img src="<?= base_url() ?>assets/images/2.jpg" class="img-responsive" alt="music theme" /></a></div>
				<div class="col-xs-12  col-sm-6 col-lg-5 "><h5><a href="#" data-toggle="modal" data-target="#blogdetail">Christmas Eve Party</a></h5><p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future.</p></div>
				<div class="col-xs-12  col-sm-3 col-lg-3 date"><b>2014</b><span>Dec 28</span></div>
			</div>
			</li>
			<li>
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-lg-4"><a href="#" data-toggle="modal" data-target="#blogdetail"><img src="<?= base_url() ?>assets/images/3.jpg" class="img-responsive" alt="music theme" /></a></div>
				<div class="col-xs-12  col-sm-6 col-lg-5 "><h5><a href="#" data-toggle="modal" data-target="#blogdetail">Christmas Eve Party</a></h5><p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future.</p></div>
				<div class="col-xs-12  col-sm-3 col-lg-3 date"><b>2014</b><span>Dec 28</span></div>
			</div>
			</li>
			<li>
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-lg-4"><a href="#" data-toggle="modal" data-target="#blogdetail"><img src="<?= base_url() ?>assets/images/4.jpg" class="img-responsive" alt="music theme" /></a></div>
				<div class="col-xs-12  col-sm-6 col-lg-5 "><h5><a href="#" data-toggle="modal" data-target="#blogdetail">Christmas Eve Party</a></h5><p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future.</p></div>
				<div class="col-xs-12  col-sm-3 col-lg-3 date"><b>2014</b><span>Dec 28</span></div>
			</div>
			</li>
		</ul>
		</div>
	</div>
	<!-- events -->

	<!-- blog -->
	<div class="col-md-5 col-md-offset-1 col-xs-12">
		<div class="ourblog">
		<h3><span class="glyphicon glyphicon-book"></span> Blog</h3>
            <ul class="row">
              <li class="row"><a href="#" data-toggle="modal" data-target="#blogdetail" class="col-xs-12 col-sm-3 col-lg-4"><img src="<?= base_url() ?>assets/images/3.jpg" class="img-responsive" alt="music theme" /></a>
              <div class="blogtext col-xs-12 col-sm-9  col-lg-8"><h5><a href="#" data-toggle="modal" data-target="#blogdetail">New trends in music</a></h5>
              <small>Posted on: Jan 1, 2014</small>
              <p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future. </p>
              </div>              
              </li>
              <li class="row"><a href="#" data-toggle="modal" data-target="#blogdetail" class="col-xs-12 col-sm-3 col-lg-4"><img src="<?= base_url() ?>assets/images/4.jpg" class="img-responsive" alt="music theme" /></a>
              <div class="blogtext col-xs-12 col-sm-9  col-lg-8"><h5><a href="#" data-toggle="modal" data-target="#blogdetail">New trends in music</a></h5>
              <small>Posted on: Jan 1, 2014</small>
              <p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future. </p>
              </div>              
              </li>
              <li class="row"><a href="#" data-toggle="modal" data-target="#blogdetail" class="col-xs-12 col-sm-3 col-lg-4"><img src="<?= base_url() ?>assets/images/1.jpg" class="img-responsive" alt="music theme" /></a>
              <div class="blogtext col-xs-12 col-sm-9  col-lg-8"><h5><a href="#" data-toggle="modal" data-target="#blogdetail">New trends in music</a></h5>
              <small>Posted on: Jan 1, 2014</small>
              <p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future. </p>
              </div>              
              </li>
               <li class="row"><a href="#" data-toggle="modal" data-target="#blogdetail" class="col-xs-12 col-sm-3 col-lg-4"><img src="<?= base_url() ?>assets/images/2.jpg" class="img-responsive" alt="music theme" /></a>
              <div class="blogtext col-xs-12 col-sm-9  col-lg-8"><h5><a href="#" data-toggle="modal" data-target="#blogdetail">New trends in music</a></h5>
              <small>Posted on: Jan 1, 2014</small>
              <p>Typewriter photo booth vinyl post-ironic, literally keffiyeh locavore tofu lomo pug Odd Future. </p>
              </div>              
              </li>          
            </ul>
            </div>

	</div>
	<!-- blog -->

</div>
</div>
<!--Blog Events Ends-->


<!--Contact Starts-->
<div id="contact" class="spacer">
<div class="contactform center">
<h3><span class="glyphicon glyphicon-envelope"></span> Contact</h3>
  <div class="row">      
      <div class="col-sm-6 col-sm-offset-3 ">
      <h4>Get in touch or<br><b>Just say Hello!</b></h4>
        <input type="text" placeholder="Name">
        <input type="text" placeholder="Email">
        <textarea rows="5" placeholder="Message"></textarea>
        <button class="btn btn-warning bgcolor">Send</button>
      </div>
  </div>


<!-- map -->
<div class="map clearfix">
<div class="fb-like-box" data-href="https://www.facebook.com/thebootstrapthemes" data-width="100%" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
</div>
<!-- map -->

</div>
<!--Contact Ends-->
</div>


<!-- Modal -->
<div class="modal fade" id="blogdetail" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>


<div id="blog">
<h2>New trends in music</h2>
<small>Posted on: Jan 20, 2013</small>
      
      <a href="#" class="thumbnail"><img src="assets/images/1.jpg" class="img-reponsive" alt="blog" /></a>
      
      <div class="col-lg-8 col-lg-offset-2">                                                                           
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      </div>

                            
</div>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<!-- Footer Starts -->
<!-- # Footer Ends -->


<!-- background slider -->

<!-- background slider -->







<!-- Modal -->
<div class="modal fade" id="albumdetail" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        


<h2>Crazy Fresco</h2>
<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/24314082&amp;theme_color=000000&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false"></iframe>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->